import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule,HttpHeaders} from '@angular/common/http';
import { LayoutRoutingModule } from './layout-routing.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './Page/home/home.component';
import { ListDeMonComponent } from './Page/list-de-mon/list-de-mon.component';
import { DeCauHoiComponent } from './Page/de-cau-hoi/de-cau-hoi.component';
import { LoginComponent } from './Page/login/login.component';
import { FormsModule } from '@angular/forms'; 
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RegisterComponent } from './Page/register/register.component';
import { ThiComponent } from './Page/thi/thi.component';
import { ProfileComponent } from './Page/profile/profile.component';
import { KetquaComponent } from './Page/ketqua/ketqua.component';


@NgModule({
  declarations: [
    //ListDeMonComponent,
  
    //DeCauHoiComponent
  
    //LoginComponent
  
    //RegisterComponent
  
    ThiComponent,
    ProfileComponent,
    KetquaComponent
  ],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    //RouterModule.forChild(Mainroutes)
  ],
 
})
export class LayoutModule { }
