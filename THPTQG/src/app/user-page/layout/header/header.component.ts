import { Component, Input, OnInit } from '@angular/core';
import { Mon } from 'src/app/_Shared/Mon/mon.model';
import { MonService } from 'src/app/_Services/Mon/mon.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { UserService } from 'src/app/_Services/User/user.service';
import { Nguoidung } from 'src/app/_Shared/NguoiDung/nguoidung.model';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  mon:Mon[];
  @Input()   data_getDN:Nguoidung[]

  constructor(private MonService:MonService,private router:Router,private userService:UserService) { }

  token=localStorage.getItem('auth-user-token');
  user=localStorage.getItem('auth-user');

  ngOnInit(): void {
    this.getAllMon();
    this.getDN();
  }
  
  showMyContainer: boolean = false;

  getAllMon(){
    this.MonService.getAllMon().subscribe((res:any) => {
      this.mon=res
    });
  }

  getDN(){
    if(this.user){
      this.userService.getByName(this.user).subscribe((res: any) => {
           this.data_getDN = res;
           console.log(this.data_getDN);
          });    
    }
  }

  Logout(){
    localStorage.clear();
    window.location.reload(); 
    this.router.navigate(['/'])
  }
}
