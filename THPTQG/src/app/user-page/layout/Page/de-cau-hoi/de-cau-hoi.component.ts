import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CauhoiDeCauHoi } from 'src/app/_Shared/CauHoi/cauhoi-de-cau-hoi.model';
import { CauHoiDeCauHoiService } from 'src/app/_Services/CauHoi/cau-hoi-de-cau-hoi.service';


@Component({
  selector: 'app-de-cau-hoi',
  templateUrl: './de-cau-hoi.component.html',
  styleUrls: ['./de-cau-hoi.component.css']
})
export class DeCauHoiComponent implements OnInit {

  CauHoiDeCauHoi: CauhoiDeCauHoi[];
  AlldapAn:any[]=[];
  dapAn:any[]=[];
  tempArray:any[]= [];
  constructor(private CauHoiDeCauHoiService:CauHoiDeCauHoiService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.getRoute(this.route.snapshot.params['maDe']);
  }

  getRoute(maDe: any) {
    this.CauHoiDeCauHoiService.GetCauHoiByMaDe(maDe).subscribe((res: any) => {
      this.CauHoiDeCauHoi=res;
      this.CauHoiDeCauHoi = this.CauHoiDeCauHoi.map((v)=>{
        //v.dapAn = Object.values(JSON.parse(v.dapAn));
       var dapan = JSON.parse(v.dapAn);
     
      for(let i in dapan){
        this.AlldapAn.push(dapan[i].mo_ta);
        // console.log("i",this.AlldapAn);
      
        //console.log('abc',this.tempArray);
     //   this.dapAn=this.AlldapAn.


      //   for(let item in dapan[i]){
      //    // this.dapAn.push(a[i].mo_ta);
      //  // console.log( a[i].mo_ta)
       
      //   }
      }
     
        return v;  
      }); 

      for (let i = 0; i < this.AlldapAn.length; i += 4) {
        let myChunk = this.AlldapAn.slice(i, i+4);
      
        this.tempArray.push(myChunk);
      }
      console.log("i", this.tempArray);
      console.log(this.CauHoiDeCauHoi);

      }, error => {
        console.log("Error", error);
      });
  }


}
