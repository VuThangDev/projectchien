import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeCauHoiComponent } from './de-cau-hoi.component';

describe('DeCauHoiComponent', () => {
  let component: DeCauHoiComponent;
  let fixture: ComponentFixture<DeCauHoiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeCauHoiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeCauHoiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
