import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CauhoiDeCauHoi } from 'src/app/_Shared/CauHoi/cauhoi-de-cau-hoi.model';
import { CauHoiDeCauHoiService } from 'src/app/_Services/CauHoi/cau-hoi-de-cau-hoi.service';
import { nguoiDungDe } from 'src/app/_Shared/NguoiDungDe/nguoidung.model';
import { Nguoidung } from 'src/app/_Shared/NguoiDung/nguoidung.model';
import { UserService } from 'src/app/_Services/User/user.service';
import { nguoiDungDeService } from 'src/app/_Services/nguoiDungDe/nguoiDungDe.service';

@Component({
  selector: 'app-thi',
  templateUrl: './thi.component.html',
  styleUrls: ['./thi.component.css']
})
export class ThiComponent implements OnInit {

  CauHoiDeCauHoi: CauhoiDeCauHoi[];
  AlldapAn:any[]=[];
  dapAn:any[]=[];
  tempArray:any[]= [];

  result:any[] = []
  total=0;
  arr:{[key:number]:any}     
  id_cauhoi:number;      
  dl:nguoiDungDe={id:0,nguoiDungId:0,maDe:0,kquaThi:'',diem:0,trangThai:1,ngayTao:new Date,ngaySua:new Date}   
data_NDD:nguoiDungDe[]=[];


  data_getDN:Nguoidung[]
  token=localStorage.getItem('auth-user-token');
  user=localStorage.getItem('auth-user');


  constructor(
    private CauHoiDeCauHoiService:CauHoiDeCauHoiService, 
    private route:ActivatedRoute,
    private userService:UserService,
    private nguoiDungDeServices:nguoiDungDeService
    ) { }

  ngOnInit(): void {
    this.getRoute(this.route.snapshot.params['maDe']);
    this.getDN();
   // this. chonDapAn(mota:string)
  }

  getRoute(maDe: any) {
    this.CauHoiDeCauHoiService.GetCauHoiByMaDe(maDe).subscribe((res: any) => {
      this.CauHoiDeCauHoi=res;
      this.CauHoiDeCauHoi = this.CauHoiDeCauHoi.map((v)=>{
        //v.dapAn = Object.values(JSON.parse(v.dapAn));
       var dapan = JSON.parse(v.dapAn);
     console.log(dapan);
      for(let i in dapan){
        this.AlldapAn.push(dapan[i].mo_ta,dapan[i].dap_an);     
      }
     
        return v;  
      }); 

      for (let i = 0; i < this.AlldapAn.length; i += 8) {
        let myChunk = this.AlldapAn.slice(i, i+8);
      
        this.tempArray.push(myChunk);

      }
      console.log("i", this.tempArray);
      console.log(this.CauHoiDeCauHoi);

      }, error => {
        console.log("Error", error);
      });
  }
   
  chonDapAn(id:number,mota:string,dapAn:string){
    // this.arr[id as keyof typeof this.arr]={};
    sessionStorage.removeItem("baiThi");
    //sessionStorage.setItem("baiThi", token);

    let tmp = true
    if(this.result.length != 0){
      for(let i = 0; i< this.result.length; i++){
        if(id == this.result[i].id){
          this.result[i].id = id
          this.result[i].mota = mota
          this.result[i].dapan = dapAn
          tmp =false
          break;
        }
      }
      if(tmp){
        this.result.push({'id':id,'mota':mota,'dapan':dapAn})        
      }

    }
    else{
      this.result.push({'id':id,'mota':mota,'dapan':dapAn})   
    }

    console.log(this.result);
  //  console.log(this.arr);
  }

  getDN(){
    if(this.user){
      this.userService.getByName(this.user).subscribe((res: any) => {
           this.data_getDN = res;
           console.log(this.data_getDN[0].ten);
          });    
    }
  }
  tinh(){
    
    if(this.result.length<40){
      alert("Bạn cần hoàn thành bài làm trước khi nộp")
    }
    else{
      for(let i = 0; i < this.result.length; i++){
        if(this.result[i].dapan){
          this.total += 0.25;
        }
      }
      this.dl={id:0,nguoiDungId: this.data_getDN[0].id,maDe:2,kquaThi:'',diem:this.total,trangThai:1,ngayTao:new Date,ngaySua:new Date}   
      this.nguoiDungDeServices.addNguoiDungDe(this.dl).subscribe(datas=>{
        this.data_NDD.push(datas)
      })
      alert('Hoàn thành, điểm của bạn là: '+this.total)
      window.location.href=('/');
    } 

  }
}
