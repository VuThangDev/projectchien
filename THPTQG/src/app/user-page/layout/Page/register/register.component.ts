import { Component, OnInit } from '@angular/core';
import { Nguoidung } from 'src/app/_Shared/NguoiDung/nguoidung.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  nguoidung: Nguoidung;

  constructor() { }

  ngOnInit(): void {
  }

  resetFrom(form:NgForm){
    form.reset();
    this.nguoidung ={
      id:0,
      ten:'',
      email: '',
      matKhau: '',
      sdt: '',
      ngaySinh: '',
      queQuan: '',
      anhdd:''
    }
  }

}
