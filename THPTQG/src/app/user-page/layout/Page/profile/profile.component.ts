import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/_Services/User/user.service';
import { Nguoidung } from 'src/app/_Shared/NguoiDung/nguoidung.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  data_getDN:Nguoidung[]
  token=localStorage.getItem('auth-user-token');
  user=localStorage.getItem('auth-user');

  constructor(private userService:UserService) { }

  ngOnInit(): void {
    this.getDN()
  }
  getDN(){
    if(this.user){
      this.userService.getByName(this.user).subscribe((res: any) => {
           this.data_getDN = res;
           console.log(this.data_getDN[0].ten);
          });    
    }
  }
  putProfile(){
    
  }
}
