import { Component, OnInit } from '@angular/core';
import { nguoiDungDeService } from 'src/app/_Services/nguoiDungDe/nguoiDungDe.service';
import { nguoiDungDe } from 'src/app/_Shared/NguoiDungDe/nguoidung.model';

@Component({
  selector: 'app-ketqua',
  templateUrl: './ketqua.component.html',
  styleUrls: ['./ketqua.component.css']
})
export class KetquaComponent implements OnInit {

  data_NDD:nguoiDungDe[]
  constructor(
    private nguoiDungDe:nguoiDungDeService
  ) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll(){
    this.nguoiDungDe.getAll().subscribe((res:any)=>{
        this.data_NDD=res.data;
        console.log(res.data);
    })
  }
}
