import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { AuthService } from 'src/app/_Services/Auth/auth.service'; 
import { TokenStorageService } from 'src/app/_Services/Auth/token-storage.service'; 
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: any = {
    EmailCus: null,
    password: null
  };
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  token:string[]
  roles: string[] = [];
  user:{email:string,passWord:string}
  constructor(
    private authService: AuthService,
    private tokenStorage: TokenStorageService,
    private _location: Location,
    private route: Router
  ) { }

  ngOnInit(): void {
  }
  
  onSubmit(): void {

    const { EmailCus,password } = this.form;
    this.authService.loginUser(EmailCus, password).subscribe(    
       (data) => {
         console.log(EmailCus);
         
         this.tokenStorage.saveTokenUser(data.token);
         this.tokenStorage.saveUser(data.users);
         //this.route.navigate(['/']);
         window.location.href=('/');
       },
       (err) => {
         console.log("d2")
               this.errorMessage = err.error.message;
               this.isLoginFailed = true;
       }


     );

  }
}
