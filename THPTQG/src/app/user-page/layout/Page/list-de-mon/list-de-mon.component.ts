import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { De } from 'src/app/_Shared/De/de.model';
import { DeService } from 'src/app/_Services/De/de.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-list-de-mon',
  templateUrl: './list-de-mon.component.html',
  styleUrls: ['./list-de-mon.component.css']
})
export class ListDeMonComponent implements OnInit {

  De: De[];

  constructor(private DeService:DeService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.getRoute(this.route.snapshot.params['maMon']);
  }

  getRoute(maMon: any) {
    this.DeService.find(maMon).subscribe((res: any) => {
      this.De=res;
      console.log(this.De);
      }, error => {
        console.log("Error", error);
      });
  }

}
