import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDeMonComponent } from './list-de-mon.component';

describe('ListDeMonComponent', () => {
  let component: ListDeMonComponent;
  let fixture: ComponentFixture<ListDeMonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListDeMonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDeMonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
