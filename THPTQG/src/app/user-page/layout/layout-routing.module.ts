import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { HomeComponent } from './Page/home/home.component';
import { ListDeMonComponent } from './Page/list-de-mon/list-de-mon.component';
import { DeCauHoiComponent } from './Page/de-cau-hoi/de-cau-hoi.component';
import { LoginComponent } from './Page/login/login.component';
import { RegisterComponent } from './Page/register/register.component';
import { ThiComponent } from './Page/thi/thi.component';
import { ProfileComponent } from './Page/profile/profile.component';
import { KetquaComponent } from './Page/ketqua/ketqua.component';

const routes: Routes = [
  { 
    path: '',component: LayoutComponent, children: [
      {path: '', component: HomeComponent},
      {path: 'DebyMon/:maMon', component: ListDeMonComponent},
      {path: 'DeCauHoi/:maDe', component: DeCauHoiComponent},
      {path: 'Dangnhap', component: LoginComponent},
      {path: 'Dangky', component: RegisterComponent},
      {path: 'Profile', component: ProfileComponent},
      {path: 'KQ', component: KetquaComponent},
      {path: 'Thi/:maDe', component: ThiComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
