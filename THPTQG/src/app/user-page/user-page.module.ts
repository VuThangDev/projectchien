import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { UserPageRoutingModule } from './user-page-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { Mon } from '../_Shared/Mon/mon.model';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HomeComponent } from './layout/Page/home/home.component';
import { ListDeMonComponent } from './layout/Page/list-de-mon/list-de-mon.component';
import { DeCauHoiComponent } from './layout/Page/de-cau-hoi/de-cau-hoi.component';
import { LoginComponent } from './layout/Page/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './layout/Page/register/register.component';

@NgModule({
  declarations: [
    LayoutComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    ListDeMonComponent,
    DeCauHoiComponent,
    LoginComponent,
    RegisterComponent,
    
  ],
  imports: [
    CommonModule,
    UserPageRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class UserPageModule { }
