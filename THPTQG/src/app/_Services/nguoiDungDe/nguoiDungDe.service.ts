import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Mon } from 'src/app/_Shared/Mon/mon.model'; 
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Nguoidung } from 'src/app/_Shared/NguoiDung/nguoidung.model';
import { nguoiDungDe } from 'src/app/_Shared/NguoiDungDe/nguoidung.model';

@Injectable({
  providedIn: 'root'
})
export class nguoiDungDeService {

  constructor(private http: HttpClient) { }

 
  addNguoiDungDe(data:nguoiDungDe):Observable<nguoiDungDe>{
    return this.http.post<nguoiDungDe>(environment.apiUrl+'/NguoiDungDes',data);
  }
  getAll():Observable<nguoiDungDe>{
    return this.http.get<nguoiDungDe>(environment.apiUrl+'/NguoiDungDes');
  }
}
