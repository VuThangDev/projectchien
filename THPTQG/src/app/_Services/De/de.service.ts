import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { De } from 'src/app/_Shared/De/de.model';
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DeService {

  constructor(private http:HttpClient) { }

  getAllDe():Observable<De[]>{
    return this.http.get<De[]>(environment.apiUrl+'/Des/GetAll/{page}/{pagesize}').pipe();
  }
  
  find(maMon:number):Observable<De[]>{
    return this.http.get<De[]>(environment.apiUrl+'/Des/GetDeByIdMon/'+ maMon).pipe();
  }

 
}
