import { TestBed } from '@angular/core/testing';

import { DeService } from './de.service';

describe('DeService', () => {
  let service: DeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
