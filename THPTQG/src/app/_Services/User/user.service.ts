import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Mon } from 'src/app/_Shared/Mon/mon.model'; 
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Nguoidung } from 'src/app/_Shared/NguoiDung/nguoidung.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getByName(name:string) {
    return this.http.get<Nguoidung[]>(environment.apiUrl+'/NguoiDungs/getData/'+name);
  }
}
