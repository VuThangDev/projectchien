import { TestBed } from '@angular/core/testing';

import { CauHoiDeCauHoiService } from './cau-hoi-de-cau-hoi.service';

describe('CauHoiDeCauHoiService', () => {
  let service: CauHoiDeCauHoiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CauHoiDeCauHoiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
