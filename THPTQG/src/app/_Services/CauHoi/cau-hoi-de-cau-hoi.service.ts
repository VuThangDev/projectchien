import { Injectable } from '@angular/core';
import { Observable, observable } from 'rxjs';
import { CauhoiDeCauHoi } from 'src/app/_Shared/CauHoi/cauhoi-de-cau-hoi.model';
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CauHoiDeCauHoiService {

  constructor(private http: HttpClient) { }

  // find(maMon:number):Observable<De[]>{
  //   return this.http.get<De[]>(environment.apiUrl+'/Des/GetDeByIdMon/'+ maMon).pipe();
  // }
  GetCauHoiByMaDe(maDe:number):Observable<CauhoiDeCauHoi[]>{
    return this.http.get<CauhoiDeCauHoi[]>(environment.apiUrl+'/CauHois/GetCauHoiByMaDe/'+ maDe).pipe();
  }
}
