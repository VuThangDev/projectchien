import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Mon } from 'src/app/_Shared/Mon/mon.model'; 
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MonService {

  constructor(private http: HttpClient) { }

  getAllMon():Observable<Mon[]>{
    return this.http.get<Mon[]>(environment.apiUrl+'/Mons/GetAllMon').pipe();
  }
}
