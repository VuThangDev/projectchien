export class Mon {
    id:number;
    tenMon:string;
    thoiLuong:number;
    soLuongCau:number;
    trangThai:number;
    ngayTao:Date;
    ngaySua:Date;
    nguoiTao:string;
    nguoiSua:string;
}

