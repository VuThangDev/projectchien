export class CauhoiDeCauHoi {
    id:number;
    maMon:number;
    maDeCauHoi:number;
    cauHoi1:number;
    anh:string;
    dapAn:string;
    trangThai:number;
    ngayTao:Date;
    ngaySua: Date;
    nguoiTao: Date;
    nguoiSua: string;
}
