﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Web_APi_THPTQG.THPTQGModel;

namespace Web_APi_THPTQG.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DesController : ControllerBase
    {
        private readonly THPTQGContext _context;

        public DesController(THPTQGContext context)
        {
            _context = context;
        }

        // GET: api/Des
        [HttpGet]
        [Route("GetAll/{page}/{pagesize}")]
        public async Task<ActionResult<IEnumerable<De>>> GetDe(int page, int pagesize)
        {
            string sp = "exec De_GetAll 1," + page + "," + pagesize + ",NULL,NULL,NULL";
            return await _context.De.FromSqlRaw(sp).ToListAsync();
        }

        // GET: api/Des/5
        [HttpGet]
        [Route("GetDeByIdMon/{maMon}")]
        public async Task<ActionResult<IEnumerable<De>>> GetDeByIdMon(int maMon)
        {
            string sp= "exec De_GetList_By_maMon " + maMon + ",1,1,10,NULL,NULL,NULL";
            //string sp = "exec De_GetList_By_maMon " + search + ",1," + page + "," + pagesize + ",NULL,NULL,NULL";
            var de = await _context.De.FromSqlRaw(sp).ToListAsync();

            if (de == null)
            {
                return NotFound();
            }

            return de;
        }

        // GET: api/Des/5
        [HttpGet("{id}")]
        public async Task<ActionResult<De>> GetDe(int id)
        {
            var de = await _context.De.FindAsync(id);

            if (de == null)
            {
                return NotFound();
            }

            return de;
        }

        // PUT: api/Des/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDe(int id, De de)
        {
            if (id != de.Id)
            {
                return BadRequest();
            }

            _context.Entry(de).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Des
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<De>> PostDe(De de)
        {
            _context.De.Add(de);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDe", new { id = de.Id }, de);
        }

        // DELETE: api/Des/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<De>> DeleteDe(int id)
        {
            var de = await _context.De.FindAsync(id);
            if (de == null)
            {
                return NotFound();
            }

            _context.De.Remove(de);
            await _context.SaveChangesAsync();

            return de;
        }

        private bool DeExists(int id)
        {
            return _context.De.Any(e => e.Id == id);
        }
    }
}
