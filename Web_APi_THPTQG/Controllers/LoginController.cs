﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Web_APi_THPTQG.THPTQGModel;

namespace Web_APi_THPTQG.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private IConfiguration _config;
        private readonly THPTQGContext _context;

        public LoginController(THPTQGContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        [HttpGet, Route("/api/Login/LoginUser")]
        public IActionResult LoginUser(string email, string password)
        {
            NguoiDung login = new NguoiDung();
            login.Email = email; login.MatKhau = password;
            IActionResult response = Unauthorized();
            var user = AuthenticateUser(login);
            if (user != null)
            {
                var tokenStr = GenerateJSONWebToken(user);
                var tenNguoidung = login.Email;
                response = Ok(new { token = tokenStr, users = tenNguoidung });
            }
            return response;
        }
        private NguoiDung AuthenticateUser(NguoiDung login)
        {
            NguoiDung user = null;
            var checkUser = _context.NguoiDung.Where(s => s.Email == login.Email && s.MatKhau == login.MatKhau).FirstOrDefault();
            if (checkUser != null)
            {
                user = new NguoiDung { Email = login.Email, MatKhau = login.MatKhau, Ten = login.Ten };
            }
            return user;
        }

        private string GenerateJSONWebToken(NguoiDung userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, userInfo.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                //new Claim(JwtRegisteredClaimNames.Aud, _config["Jwt:Audience"]),
                //new Claim(JwtRegisteredClaimNames.Iss, _config["Jwt:Issuer"])
            };
            var token = new JwtSecurityToken(
                issuer: _config["Jwt:Issuer"],
                audience: _config["Jwt:Issuer"],
                claims,
                expires: DateTime.Now.AddMinutes(180),
                signingCredentials: credentials
            );
            var encodetoken = new JwtSecurityTokenHandler().WriteToken(token);
            return encodetoken;
        }
        //[Authorize]
        //[HttpPost("Post")]
        //public string Post()
        //{
        //    var identity = HttpContext.User.Identity as ClaimsIdentity;
        //    IList<Claim> claim = identity.Claims.ToList();
        //    var Ten = claim[0].Value;
        //    return "User " + Ten + ": Login Successfully!";
        //}
        //[Authorize] //example
        //[HttpGet("GetValue")]
        //public async Task<ActionResult<IEnumerable<string>>> Get()
        //{
        //    return new string[] { "Value1", "Value2", "Value3" };
        //}
    }
}
