﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Web_APi_THPTQG.THPTQGModel;

namespace Web_APi_THPTQG.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MonsController : ControllerBase
    {
        private readonly THPTQGContext _context;


        public MonsController(THPTQGContext context)
        {
            _context = context;
        }

        // GET: api/Mons
        [HttpGet]
        [Route("GetAllMon")]
        public async Task<ActionResult<IEnumerable<Mon>>> GetAllMon()
        {
            string sp = "exec Mon_Getall";
            return await _context.Mon.FromSqlRaw(sp).ToListAsync();
        }

        // GET: api/Mons/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Mon>> GetMon(int id)
        {
            var mon = await _context.Mon.FindAsync(id);

            if (mon == null)
            {
                return NotFound();
            }

            return mon;
        }

        // PUT: api/Mons/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMon(int id, Mon mon)
        {
            if (id != mon.Id)
            {
                return BadRequest();
            }

            _context.Entry(mon).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MonExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Mons
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Mon>> PostMon(Mon mon)
        {
            _context.Mon.Add(mon);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMon", new { id = mon.Id }, mon);
        }

        // DELETE: api/Mons/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Mon>> DeleteMon(int id)
        {
            var mon = await _context.Mon.FindAsync(id);
            if (mon == null)
            {
                return NotFound();
            }

            _context.Mon.Remove(mon);
            await _context.SaveChangesAsync();

            return mon;
        }

        private bool MonExists(int id)
        {
            return _context.Mon.Any(e => e.Id == id);
        }
    }
}
