﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Web_APi_THPTQG.THPTQGModel;

namespace Web_APi_THPTQG.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CauHoisController : ControllerBase
    {
        private readonly THPTQGContext _context;

        public CauHoisController(THPTQGContext context)
        {
            _context = context;
        }

        // GET: api/CauHois
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CauHoi>>> GetCauHoi()
        {
            return await _context.CauHoi.ToListAsync();
        }

        // GET: api/CauHois/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CauHoi>> GetCauHoi(int id)
        {
            var cauHoi = await _context.CauHoi.FindAsync(id);

            if (cauHoi == null)
            {
                return NotFound();
            }

            return cauHoi;
        }

        [HttpGet]
        [Route("GetCauHoiByMaDe/{MaDe}")]
        public async Task<ActionResult<IEnumerable<CauHoi>>> GetCauHoiByMaDe(int MaDe)
        {
            string sp = "exec CauHoi_GetList_By_maDe " + MaDe + ",1,NULL,NULL";
            //string sp = "exec De_GetList_By_maMon " + maMon + ",1,1,10,NULL,NULL,NULL";
            var cauhoi = await _context.CauHoi.FromSqlRaw(sp).ToListAsync();

            if (cauhoi == null)
            {
                return NotFound();
            }

            return cauhoi;
        }

        // PUT: api/CauHois/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCauHoi(int id, CauHoi cauHoi)
        {
            if (id != cauHoi.Id)
            {
                return BadRequest();
            }

            _context.Entry(cauHoi).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CauHoiExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CauHois
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CauHoi>> PostCauHoi(CauHoi cauHoi)
        {
            _context.CauHoi.Add(cauHoi);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCauHoi", new { id = cauHoi.Id }, cauHoi);
        }

        // DELETE: api/CauHois/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CauHoi>> DeleteCauHoi(int id)
        {
            var cauHoi = await _context.CauHoi.FindAsync(id);
            if (cauHoi == null)
            {
                return NotFound();
            }

            _context.CauHoi.Remove(cauHoi);
            await _context.SaveChangesAsync();

            return cauHoi;
        }

        private bool CauHoiExists(int id)
        {
            return _context.CauHoi.Any(e => e.Id == id);
        }
    }
}
