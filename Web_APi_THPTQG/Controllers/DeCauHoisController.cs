﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Web_APi_THPTQG.THPTQGModel;

namespace Web_APi_THPTQG.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeCauHoisController : ControllerBase
    {
        private readonly THPTQGContext _context;

        public DeCauHoisController(THPTQGContext context)
        {
            _context = context;
        }

        // GET: api/DeCauHois
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DeCauHoi>>> GetDeCauHoi()
        {
            return await _context.DeCauHoi.ToListAsync();
        }

        // GET: api/DeCauHois/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DeCauHoi>> GetDeCauHoi(int id)
        {
            var deCauHoi = await _context.DeCauHoi.FindAsync(id);

            if (deCauHoi == null)
            {
                return NotFound();
            }

            return deCauHoi;
        }

        // PUT: api/DeCauHois/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDeCauHoi(int id, DeCauHoi deCauHoi)
        {
            if (id != deCauHoi.Id)
            {
                return BadRequest();
            }

            _context.Entry(deCauHoi).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DeCauHoiExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DeCauHois
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<DeCauHoi>> PostDeCauHoi(DeCauHoi deCauHoi)
        {
            _context.DeCauHoi.Add(deCauHoi);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDeCauHoi", new { id = deCauHoi.Id }, deCauHoi);
        }

        // DELETE: api/DeCauHois/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<DeCauHoi>> DeleteDeCauHoi(int id)
        {
            var deCauHoi = await _context.DeCauHoi.FindAsync(id);
            if (deCauHoi == null)
            {
                return NotFound();
            }

            _context.DeCauHoi.Remove(deCauHoi);
            await _context.SaveChangesAsync();

            return deCauHoi;
        }

        private bool DeCauHoiExists(int id)
        {
            return _context.DeCauHoi.Any(e => e.Id == id);
        }
    }
}
