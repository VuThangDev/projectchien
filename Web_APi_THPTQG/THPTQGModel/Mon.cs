﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Web_APi_THPTQG.THPTQGModel
{
    public partial class Mon
    {
        public Mon()
        {
            CauHoi = new HashSet<CauHoi>();
            De = new HashSet<De>();
        }

        public int Id { get; set; }
        public string TenMon { get; set; }
        public int? ThoiLuong { get; set; }
        public int? SoLuongCau { get; set; }
        public int? TrangThai { get; set; }
        public DateTime? NgayTao { get; set; }
        public DateTime? NgaySua { get; set; }
        public string NguoiTao { get; set; }
        public string NguoiSua { get; set; }

        public virtual ICollection<CauHoi> CauHoi { get; set; }
        public virtual ICollection<De> De { get; set; }
    }
}
