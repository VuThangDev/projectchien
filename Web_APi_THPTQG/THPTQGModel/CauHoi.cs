﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Web_APi_THPTQG.THPTQGModel
{
    public partial class CauHoi
    {
        public CauHoi()
        {
            DeCauHoi = new HashSet<DeCauHoi>();
        }

        public int Id { get; set; }
        public int? MaMon { get; set; }
        public int? MaDeCauHoi { get; set; }
        public string CauHoi1 { get; set; }
        public string Anh { get; set; }
        public string DapAn { get; set; }
        public int? TrangThai { get; set; }
        public DateTime? NgayTao { get; set; }
        public DateTime? NgaySua { get; set; }
        public string NguoiTao { get; set; }
        public string NguoiSua { get; set; }

        public virtual Mon MaMonNavigation { get; set; }
        public virtual ICollection<DeCauHoi> DeCauHoi { get; set; }
    }
}
