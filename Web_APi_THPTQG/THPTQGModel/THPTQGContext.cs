﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Web_APi_THPTQG.THPTQGModel
{
    public partial class THPTQGContext : DbContext
    {
        public THPTQGContext()
        {
        }

        public THPTQGContext(DbContextOptions<THPTQGContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CauHoi> CauHoi { get; set; }
        public virtual DbSet<DanToc> DanToc { get; set; }
        public virtual DbSet<De> De { get; set; }
        public virtual DbSet<DeCauHoi> DeCauHoi { get; set; }
        public virtual DbSet<LogsAction> LogsAction { get; set; }
        public virtual DbSet<LogsError> LogsError { get; set; }
        public virtual DbSet<Mon> Mon { get; set; }
        public virtual DbSet<NguoiDung> NguoiDung { get; set; }
        public virtual DbSet<NguoiDungDe> NguoiDungDe { get; set; }
        public virtual DbSet<NguoiQuanLi> NguoiQuanLi { get; set; }
        public virtual DbSet<QuanHuyen> QuanHuyen { get; set; }
        public virtual DbSet<QuocGia> QuocGia { get; set; }
        public virtual DbSet<TinhThanhPho> TinhThanhPho { get; set; }
        public virtual DbSet<XaPhuong> XaPhuong { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=103.101.162.107;Initial Catalog=THPTQG;Persist Security Info=True;User ID=SA;Password=U2kqZuK0syuLN4B6");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CauHoi>(entity =>
            {
                entity.Property(e => e.Anh).HasMaxLength(250);

                entity.Property(e => e.CauHoi1)
                    .HasColumnName("CauHoi")
                    .HasColumnType("ntext");

                entity.Property(e => e.DapAn).HasColumnType("ntext");

                entity.Property(e => e.MaDeCauHoi).HasColumnName("maDe_CauHoi");

                entity.Property(e => e.MaMon).HasColumnName("maMon");

                entity.Property(e => e.NgaySua).HasColumnType("date");

                entity.Property(e => e.NgayTao).HasColumnType("date");

                entity.Property(e => e.NguoiSua).HasMaxLength(50);

                entity.Property(e => e.NguoiTao).HasMaxLength(50);

                entity.HasOne(d => d.MaMonNavigation)
                    .WithMany(p => p.CauHoi)
                    .HasForeignKey(d => d.MaMon)
                    .HasConstraintName("FK_CauHoi_Mon");
            });

            modelBuilder.Entity<DanToc>(entity =>
            {
                entity.Property(e => e.MoTa).HasMaxLength(300);

                entity.Property(e => e.TenDanToc).HasMaxLength(100);

                entity.Property(e => e.TieuDe).HasMaxLength(250);

                entity.Property(e => e.TrangThai).HasDefaultValueSql("((1))");

                entity.Property(e => e.TuKhoa).HasMaxLength(300);
            });

            modelBuilder.Entity<De>(entity =>
            {
                entity.Property(e => e.MaMon).HasColumnName("maMon");

                entity.Property(e => e.NgaySua).HasColumnType("date");

                entity.Property(e => e.NgayTao).HasColumnType("date");

                entity.Property(e => e.NguoiSua).HasMaxLength(50);

                entity.Property(e => e.NguoiTao).HasMaxLength(50);

                entity.HasOne(d => d.MaMonNavigation)
                    .WithMany(p => p.De)
                    .HasForeignKey(d => d.MaMon)
                    .HasConstraintName("FK_De_Mon");
            });

            modelBuilder.Entity<DeCauHoi>(entity =>
            {
                entity.Property(e => e.MaDe).HasColumnName("maDe");

                entity.Property(e => e.NgaySua).HasColumnType("date");

                entity.Property(e => e.NgayTao).HasColumnType("date");

                entity.Property(e => e.NguoiSua).HasMaxLength(50);

                entity.Property(e => e.NguoiTao).HasMaxLength(50);

                entity.HasOne(d => d.MaCauHoiNavigation)
                    .WithMany(p => p.DeCauHoi)
                    .HasForeignKey(d => d.MaCauHoi)
                    .HasConstraintName("FK_DeCauHoi_CauHoi");

                entity.HasOne(d => d.MaDeNavigation)
                    .WithMany(p => p.DeCauHoi)
                    .HasForeignKey(d => d.MaDe)
                    .HasConstraintName("FK_DeCauHoi_De");
            });

            modelBuilder.Entity<LogsAction>(entity =>
            {
                entity.ToTable("logs_action");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.DiaChiIp)
                    .HasColumnName("DiaChiIP")
                    .HasMaxLength(15);

                entity.Property(e => e.EffectedData)
                    .HasColumnName("effected_data")
                    .HasMaxLength(250);

                entity.Property(e => e.HanhDong).HasMaxLength(150);

                entity.Property(e => e.NgaySua).HasColumnType("datetime");

                entity.Property(e => e.NgayTao).HasColumnType("datetime");

                entity.Property(e => e.Os)
                    .HasColumnName("os")
                    .HasMaxLength(100);

                entity.Property(e => e.TenBang).HasMaxLength(150);

                entity.Property(e => e.TenTruong).HasMaxLength(150);

                entity.Property(e => e.ThietBi).HasMaxLength(50);

                entity.Property(e => e.TrinhDuyet).HasMaxLength(150);
            });

            modelBuilder.Entity<LogsError>(entity =>
            {
                entity.ToTable("logs_error");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Controller).HasMaxLength(150);

                entity.Property(e => e.DiaChiIp)
                    .HasColumnName("DiaChiIP")
                    .HasMaxLength(15);

                entity.Property(e => e.HanhDong).HasMaxLength(150);

                entity.Property(e => e.NgaySua).HasColumnType("datetime");

                entity.Property(e => e.NgayTao).HasColumnType("datetime");

                entity.Property(e => e.Os)
                    .HasColumnName("os")
                    .HasMaxLength(100);

                entity.Property(e => e.ThietBi).HasMaxLength(50);

                entity.Property(e => e.ThongTinLoi).HasMaxLength(500);

                entity.Property(e => e.TrinhDuyet).HasMaxLength(150);
            });

            modelBuilder.Entity<Mon>(entity =>
            {
                entity.Property(e => e.NgaySua).HasColumnType("date");

                entity.Property(e => e.NgayTao).HasColumnType("date");

                entity.Property(e => e.NguoiSua).HasMaxLength(50);

                entity.Property(e => e.NguoiTao).HasMaxLength(50);

                entity.Property(e => e.TenMon).HasMaxLength(50);
            });

            modelBuilder.Entity<NguoiDung>(entity =>
            {
                entity.Property(e => e.Anhdd).HasMaxLength(250);

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MatKhau)
                    .HasColumnName("matKhau")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NgaySinh).HasColumnType("date");

                entity.Property(e => e.NgaySua).HasColumnType("date");

                entity.Property(e => e.NgayTao).HasColumnType("date");

                entity.Property(e => e.NguoiSua).HasMaxLength(50);

                entity.Property(e => e.QueQuan).HasMaxLength(250);

                entity.Property(e => e.Sdt)
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Ten).HasMaxLength(70);
            });

            modelBuilder.Entity<NguoiDungDe>(entity =>
            {
                entity.Property(e => e.Diem).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.KquaThi).HasColumnType("ntext");

                entity.Property(e => e.MaDe).HasColumnName("maDe");

                entity.Property(e => e.NgaySua).HasColumnType("date");

                entity.Property(e => e.NgayTao).HasColumnType("date");

                entity.Property(e => e.NguoiDungId).HasColumnName("NguoiDung_Id");

                entity.HasOne(d => d.MaDeNavigation)
                    .WithMany(p => p.NguoiDungDe)
                    .HasForeignKey(d => d.MaDe)
                    .HasConstraintName("FK_NguoiDungDe_De");

                entity.HasOne(d => d.NguoiDung)
                    .WithMany(p => p.NguoiDungDe)
                    .HasForeignKey(d => d.NguoiDungId)
                    .HasConstraintName("FK_NguoiDungDe_NguoiDung");
            });

            modelBuilder.Entity<NguoiQuanLi>(entity =>
            {
                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HoVaTen).HasMaxLength(50);

                entity.Property(e => e.MatKhau)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NgaySua).HasColumnType("datetime");

                entity.Property(e => e.NgayTao).HasColumnType("datetime");

                entity.Property(e => e.NguoiSua).HasMaxLength(250);

                entity.Property(e => e.NguoiTao).HasMaxLength(250);

                entity.Property(e => e.SoDienThoai)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TaiKhoan)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<QuanHuyen>(entity =>
            {
                entity.HasKey(e => e.MaQuanHuyen)
                    .HasName("PK_m_districts");

                entity.Property(e => e.MaQuanHuyen)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GhiChuE)
                    .HasColumnName("GhiChu_E")
                    .HasMaxLength(250);

                entity.Property(e => e.GhiChuL)
                    .HasColumnName("GhiChu_L")
                    .HasMaxLength(250);

                entity.Property(e => e.KiHieuQuanHuyen)
                    .IsRequired()
                    .HasMaxLength(36);

                entity.Property(e => e.KinhDoViDo).HasMaxLength(50);

                entity.Property(e => e.LoaiQuanHuyenE)
                    .HasColumnName("LoaiQuanHuyen_E")
                    .HasMaxLength(20);

                entity.Property(e => e.LoaiQuanHuyenL)
                    .HasColumnName("LoaiQuanHuyen_L")
                    .HasMaxLength(20);

                entity.Property(e => e.MaTinh)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MoTa).HasMaxLength(250);

                entity.Property(e => e.NgaySua).HasColumnType("datetime");

                entity.Property(e => e.NgayTao).HasColumnType("datetime");

                entity.Property(e => e.TenQuanHuyenE)
                    .HasColumnName("TenQuanHuyen_E")
                    .HasMaxLength(100);

                entity.Property(e => e.TenQuanHuyenL)
                    .HasColumnName("TenQuanHuyen_L")
                    .HasMaxLength(100);

                entity.Property(e => e.TieuDe).HasMaxLength(250);

                entity.Property(e => e.TuKhoa).HasMaxLength(250);
            });

            modelBuilder.Entity<QuocGia>(entity =>
            {
                entity.HasKey(e => e.MaQuocGia)
                    .HasName("PK_m_countries");

                entity.Property(e => e.MaQuocGia)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Flags)
                    .HasColumnName("flags")
                    .HasMaxLength(350);

                entity.Property(e => e.GhiChuE)
                    .HasColumnName("GhiChu_E")
                    .HasMaxLength(250);

                entity.Property(e => e.GhiChuL)
                    .HasColumnName("GhiChu_L")
                    .HasMaxLength(250);

                entity.Property(e => e.KiHieu).HasMaxLength(36);

                entity.Property(e => e.KiHieuDienThoai).HasMaxLength(10);

                entity.Property(e => e.KiHieuInternet).HasMaxLength(10);

                entity.Property(e => e.KiHieuTienTe).HasMaxLength(10);

                entity.Property(e => e.MaLoaiQuocGia).HasMaxLength(50);

                entity.Property(e => e.MoTa).HasMaxLength(250);

                entity.Property(e => e.NgaySua).HasColumnType("datetime");

                entity.Property(e => e.NgayTao).HasColumnType("datetime");

                entity.Property(e => e.TenQuocGiaE)
                    .IsRequired()
                    .HasColumnName("TenQuocGia_E")
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TenQuocGiaL)
                    .IsRequired()
                    .HasColumnName("TenQuocGia_L")
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TenTienTe).HasMaxLength(10);

                entity.Property(e => e.ThuDo).HasMaxLength(50);

                entity.Property(e => e.ThuTu).HasDefaultValueSql("((0))");

                entity.Property(e => e.TieuDe).HasMaxLength(250);

                entity.Property(e => e.TuKhoa).HasMaxLength(250);
            });

            modelBuilder.Entity<TinhThanhPho>(entity =>
            {
                entity.HasKey(e => e.MaTinh)
                    .HasName("PK_m_provinces");

                entity.Property(e => e.MaTinh)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GhiChuE)
                    .HasColumnName("GhiChu_E")
                    .HasMaxLength(250)
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.GhiChuL)
                    .HasColumnName("GhiChu_L")
                    .HasMaxLength(250)
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.KiHieuTinh).HasMaxLength(36);

                entity.Property(e => e.LoaiTinhE)
                    .HasColumnName("LoaiTinh_E")
                    .HasMaxLength(50);

                entity.Property(e => e.LoaiTinhL)
                    .HasColumnName("LoaiTinh_L")
                    .HasMaxLength(50);

                entity.Property(e => e.MaQuocGia)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MoTa).HasMaxLength(250);

                entity.Property(e => e.NgaySua).HasColumnType("datetime");

                entity.Property(e => e.NgayTao).HasColumnType("datetime");

                entity.Property(e => e.TenTinhE)
                    .IsRequired()
                    .HasColumnName("TenTinh_E")
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TenTinhL)
                    .IsRequired()
                    .HasColumnName("TenTinh_L")
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.TieuDe).HasMaxLength(250);

                entity.Property(e => e.TuKhoa).HasMaxLength(250);
            });

            modelBuilder.Entity<XaPhuong>(entity =>
            {
                entity.HasKey(e => e.MaPhuongXa)
                    .HasName("PK_m_wards");

                entity.Property(e => e.MaPhuongXa)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GhiChuE)
                    .HasColumnName("GhiChu_E")
                    .HasMaxLength(250);

                entity.Property(e => e.GhiChuL)
                    .HasColumnName("GhiChu_L")
                    .HasMaxLength(250);

                entity.Property(e => e.KiHieuPhuongXa)
                    .IsRequired()
                    .HasMaxLength(36);

                entity.Property(e => e.KinhDoViDo).HasMaxLength(50);

                entity.Property(e => e.LoaiPhuongXaE)
                    .HasColumnName("LoaiPhuongXa_E")
                    .HasMaxLength(50);

                entity.Property(e => e.LoaiPhuongXaL)
                    .HasColumnName("LoaiPhuongXa_L")
                    .HasMaxLength(50);

                entity.Property(e => e.MaQuanHuyen)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MoTa).HasMaxLength(250);

                entity.Property(e => e.NgaySua).HasColumnType("datetime");

                entity.Property(e => e.NgayTao).HasColumnType("datetime");

                entity.Property(e => e.TenPhuongXaE)
                    .HasColumnName("TenPhuongXa_E")
                    .HasMaxLength(100);

                entity.Property(e => e.TenPhuongXaL)
                    .HasColumnName("TenPhuongXa_L")
                    .HasMaxLength(100);

                entity.Property(e => e.TieuDe).HasMaxLength(250);

                entity.Property(e => e.TuKhoa).HasMaxLength(250);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
