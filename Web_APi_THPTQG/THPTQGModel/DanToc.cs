﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Web_APi_THPTQG.THPTQGModel
{
    public partial class DanToc
    {
        public int Id { get; set; }
        public string TenDanToc { get; set; }
        public string TieuDe { get; set; }
        public string MoTa { get; set; }
        public string TuKhoa { get; set; }
        public int? TrangThai { get; set; }
    }
}
