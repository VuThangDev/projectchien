﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Web_APi_THPTQG.THPTQGModel
{
    public partial class NguoiDungDe
    {
        public int Id { get; set; }
        public int? NguoiDungId { get; set; }
        public int? MaDe { get; set; }
        public string KquaThi { get; set; }
        public decimal? Diem { get; set; }
        public int? TrangThai { get; set; }
        public DateTime? NgayTao { get; set; }
        public DateTime? NgaySua { get; set; }

        public virtual De MaDeNavigation { get; set; }
        public virtual NguoiDung NguoiDung { get; set; }
    }
}
