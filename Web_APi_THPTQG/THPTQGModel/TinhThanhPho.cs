﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Web_APi_THPTQG.THPTQGModel
{
    public partial class TinhThanhPho
    {
        public string MaTinh { get; set; }
        public string KiHieuTinh { get; set; }
        public string TenTinhE { get; set; }
        public string TenTinhL { get; set; }
        public int? KiHieuDienThoai { get; set; }
        public int? MaBuuChinh { get; set; }
        public string MaQuocGia { get; set; }
        public int? ThuTu { get; set; }
        public string GhiChuE { get; set; }
        public string GhiChuL { get; set; }
        public int TrangThai { get; set; }
        public Guid NguoiTao { get; set; }
        public DateTime NgayTao { get; set; }
        public DateTime? NgaySua { get; set; }
        public Guid? NguoiSua { get; set; }
        public string TieuDe { get; set; }
        public string MoTa { get; set; }
        public string TuKhoa { get; set; }
        public string LoaiTinhE { get; set; }
        public string LoaiTinhL { get; set; }
    }
}
